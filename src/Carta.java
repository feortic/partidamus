//La clase carta implementa Comparable para poder odenar las cartas de los jugadores de mayor a menor
//seg�n su n�mero
public class Carta implements Comparable<Carta>{
	
	//Creamos las varibles del numero y el palo de la carta
	private int numero;
	private String palo;
	
	public Carta(int n , String p)
	{
		this.numero = n;
		this.palo = p;
	}
	
	public int getNumero() {
		return this.numero;
	}
	
	public String getPalo()
	{
		return this.palo;
	}
	public void mostrarCarta()
	{
		System.out.println(this.toString());
	}
	public String toString()
	{
		return "El palo es: "+this.getPalo() +"el n�mero es: "+this.getNumero();
	}
	//Se implementa el metodo comparable de manera que las cartasde mayores vayan al princip�o
	public int compareTo(Carta c) {
		if(this.numero>c.getNumero()) {
			return -1;
		}
		if(this.numero<c.getNumero()) {
			return 1;
		}
		return 0;
	}
}
