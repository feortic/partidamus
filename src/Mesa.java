import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Mesa {

	//Creamos dos "mazos" de cartas, uno con las cartas para repartir y otro con las cartas que los
	//jugadores se descartan si quieren mus
	private List<Carta> cartasMonton;
	private List<Carta> cartasMus;
	
	public Mesa() {
		//Se crean las listas de cartas y las cartas, eliminandose, el 2, el 8 y el 9
		this.cartasMonton = new ArrayList<>();
		this.cartasMus = new ArrayList<>();
		
		this.cartasMonton.add(new Carta(1,"Copas"));
		this.cartasMonton.add(new Carta(1,"Bastos"));
		this.cartasMonton.add(new Carta(1,"Oros"));
		this.cartasMonton.add(new Carta(1,"Espadas"));
		for(int i = 3;i<=7;i++)
		{
			this.cartasMonton.add(new Carta(i,"Copas"));
			this.cartasMonton.add(new Carta(i,"Bastos"));
			this.cartasMonton.add(new Carta(i,"Oros"));
			this.cartasMonton.add(new Carta(i,"Espadas"));
		}
		for(int j = 10; j<=12;j++) {
			this.cartasMonton.add(new Carta(j,"Copas"));
			this.cartasMonton.add(new Carta(j,"Bastos"));
			this.cartasMonton.add(new Carta(j,"Oros"));
			this.cartasMonton.add(new Carta(j,"Espadas"));
		}
	}
	
	public boolean montonVacio()
	{
		return this.cartasMonton.isEmpty();
	}
	public Carta getCartaMonton()
	{
		Random r = new Random();
		int n = r.nextInt(this.cartasMonton.size());
		return this.cartasMonton.remove(n);
	}
	public Carta getCartaMus()
	{
		Random r = new Random();
		int n = r.nextInt(this.cartasMus.size());
		return this.cartasMus.remove(n);
	}
	
	public void añadirCartaMontonMus(Carta c)
	{
		this.cartasMus.add(c);
	}
	
	
}
