import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Jugador {
	//Creamos la lista de cartas que tiene el jugador, y un entero con a pareja a la que pertenece
	List<Carta> cartas;
	private int pareja;
	

	public Jugador(int pareja) {
		
		this.cartas = new ArrayList<>();
		this.pareja	= pareja;
		
	}
	//Este metodo ordena las cartas, ya que son del un tipo que implenta Comparable<T>
	public void ordenarCartas()
	{
		Collections.sort(this.cartas);
	}
	public String mostrarCartas()
	{
		String s="";
		for(Carta c : this.cartas)
		{
			s +=(c.getPalo()+ " "+ c.getNumero()+" ");
		}
		return s;
	}
	//a�ade a la lista de cartas del jugador las 4 cartas
	public void obtenerCartasInicial(Mesa m)
	{
		for(int i=0;i<4;i++) {
			this.cartas.add(m.getCartaMonton());
		}
	}
	
	//Pedrete: devuelve cierto si las cuatro cartas del jugador son 7,6,5,4
	
	public boolean tienePedrete()
	{
		return(this.cartas.get(0).getNumero()==7&&this.cartas.get(1).getNumero()==6&&this.cartas.get(2).getNumero()==5&&this.cartas.get(3).getNumero()==4);
	}
	
	//Este metodo devuelve el numero de cartas que el jugador quiere descartarse
	public int quiereMus()
	{
		System.out.println("�Quieres mus? [y/n]");
		Scanner entrada = new Scanner(System.in);
		String s = entrada.nextLine();
		if(s.equals("y"))
		{
			System.out.println("�Cuantas cartas quieres tirar?");
			
			return entrada.nextInt();
			
		}else{
			return 0;
		}
	}
	
	//M�todos para pares
	//Devuelve cierto, si el jugador tiene 2 cartas cuyo numero coincide y falso en otro caso
	public boolean tienePares() {

		int primera = this.cartas.get(0).getNumero();
		int segunda = this.cartas.get(1).getNumero();
		int tercera = this.cartas.get(2).getNumero();
		int cuarta = this.cartas.get(3).getNumero();
		if(primera==segunda||primera==tercera||primera==cuarta||
			segunda==tercera||segunda==cuarta||
			tercera==cuarta) {
			return true;
		}else {
			return false;
		}
	}
	//Devuelve cierto si el jugador tiene 3 cartas cuyo numero coincide y falso en otro caso
	public boolean tieneMedias()
	{
		int primera = this.cartas.get(0).getNumero();
		int segunda = this.cartas.get(1).getNumero();
		int tercera = this.cartas.get(2).getNumero();
		int cuarta = this.cartas.get(3).getNumero();
		if(primera==segunda&&segunda==tercera||primera==segunda&&segunda==cuarta||primera==tercera&&tercera==cuarta||segunda==tercera&&tercera==cuarta) {
			return true;
		}else {
			return false;
		}
	}
	//Devulve cierto, si el jugador tiene 2 cartas con el mismo numero, y las otras 2 tambien coinciden en numero entre ellas (2 parejas) y falso en otro caso
	public boolean tieneDuplex()
	{
		if(this.tienePares())
		{
			
			
			int primera = this.cartas.get(0).getNumero();
			int segunda = this.cartas.get(1).getNumero();
			int tercera = this.cartas.get(2).getNumero();
			int cuarta = this.cartas.get(3).getNumero();
			if(primera==segunda&&tercera==cuarta)
			{
				return true;
			}
			if(primera==tercera&&segunda==cuarta)
			{
				return true;
			}
			if(primera==cuarta&&segunda==tercera)
			{
				return true;
			}else {
				return false;
			}

		}
		else {
			return false;
		}
	}
	
	
	//Metodos juego
	//Devulve cierto si la suma de los numeros de las cartas suman 30 o m�s y falso en otro caso
	public boolean tieneJuego() {
		
		int suma=0;
		
		for(Carta c : this.cartas)
		{
			if(c.getNumero()==10||c.getNumero()==11||c.getNumero()==12)
			{
				suma+=10;
			}
			else {
				suma+=c.getNumero();
			}
			
		}
		if(suma>30)
		{
			return true;
		}
		else {
			return false;
		}
		
	}
	//Devuelve cierto, si el valor de la suma de las cartas del jugador es 31 y falso en otro caso
	public boolean tiene31()
	{
		if(this.tieneJuego())
		{
			int suma=0;
			for(Carta c : this.cartas)
			{
				if(c.getNumero()==10||c.getNumero()==11||c.getNumero()==12)
				{
					suma+=10;
				}
				else {
					suma+=c.getNumero();
				}
			}
			if(suma==31)
			{
				return true;
			}
			else {
				return false;
			}
		}else {
			return false;
		}
	}
	//Devuelve la suma de los puntos de las cartas del jugador, en caso de que tenga juego, si no devuelve 0
	public int getJuego() {
		if(this.tieneJuego())
		{
			int suma=0;
			for(Carta c : this.cartas)
			{
				if(c.getNumero()==10||c.getNumero()==11||c.getNumero()==12)
				{
					suma+=10;
				}
				else {
					suma+=c.getNumero();
				}
			}
			return suma;
		
		}else {
			return 0;
		}
	}
	//Devuelve la suma de los puntos de las cartas en caso de que el jugador no tenga juego, si no devuelve 0
	public int getPunto(){
		if(!this.tieneJuego())
		{
			int suma=0;
			for(Carta c : this.cartas)
			{
				if(c.getNumero()==10||c.getNumero()==11||c.getNumero()==12)
				{
					suma+=10;
				}
				else {
					suma+=c.getNumero();
				}
			}
			return suma;
		
		}else {
			return 0;
		}
	}
	public int getPareja() {
		return this.pareja;
	}
	public void eliminarCartas() {
		while(!this.cartas.isEmpty()) {
			this.cartas.remove(0);
		}
	}
}
