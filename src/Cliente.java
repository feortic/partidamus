import java.io.DataInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class Cliente {

	public static void main(String[] args) {
		Socket s = null;
		DataInputStream dis = null;
		PrintWriter pw = null;
		try {
			
			//Se crea el socket que usar� el puerto 7777
			s = new Socket("localhost",7777);
			dis = new DataInputStream(s.getInputStream());
			//El constructor PrintWriter(OutputStream, boolean), si el booleano es cierto, 
			//el printwriter hace automaticamente el flush sin necesidad de ponerlo en cada linea
			pw = new PrintWriter(s.getOutputStream(),true);
			

			Scanner entrada = new Scanner(System.in);
			String lin = dis.readLine();
			//Marca de final de la partida
			while(!lin.equals("$*")) {
				//Si empieza por Informacion: lo unico que har� ser� mostrar por pantalla, si no adem�s esperar� la respuesta del jugador
				if(lin.startsWith("Informacion: ")) {
					System.out.println(lin);
				}else {
					System.out.println(lin);
					pw.println(entrada.nextLine());
				}
				lin = dis.readLine();
			}
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}finally {
			
				try {
					if(s!=null)
						s.close();
					if(dis!=null)
						dis.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			
		}
		
	}

}
