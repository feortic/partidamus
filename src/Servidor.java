import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Servidor implements Runnable {
	//Se declaran 4 sockets, 1 por cada jugador
	private Socket sjugador1=null;
	private Socket sjugador2=null;
	private Socket sjugador3=null;
	private Socket sjugador4=null;
	
	
	public Servidor(Socket j1,Socket j2,Socket j3,Socket j4) {
		this.sjugador1 = j1;
		this.sjugador2 = j2;
		this.sjugador3 = j3;
		this.sjugador4 = j4;
	}
	//El metodo run, crea una partida con los 4 sockets de los 4 jugadores y la gestiona
	public void run() {
		Partida p = new Partida(this.sjugador1,this.sjugador2,this.sjugador3,this.sjugador4);
		p.gestionarPartida();
		
	}
	public static void main(String[] args) {

		ServerSocket ss =null;	
		ExecutorService pool =null;
		try {
			//Se crea un pool de hilos
			pool = Executors.newCachedThreadPool();
			ss= new ServerSocket(7777);
			while(true)
			{
				//Se espera a que se conecten 4 jugadores
				final Socket cliente1 = ss.accept();
				final Socket cliente2 = ss.accept();
				final Socket cliente3 = ss.accept();
				final Socket cliente4 = ss.accept();
				
				Servidor s = new Servidor(cliente1,cliente2,cliente3,cliente4);
				pool.execute(s);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}finally {
			try {
				pool.shutdown();
				if(ss!=null)
				ss.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}
}

