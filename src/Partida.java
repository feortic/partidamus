import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Partida {

	//Se declara una lista de jugadores, otra con sus respectivos OutputStream y otra con sus InputStream
	private List<Jugador> lJugadores = null;
	private List<OutputStream> escritores = null;
	private List<InputStream> lectores = null;
	//Se declara la mesa donde se jugar� la partida, los contadores de puntos por pareja...
	private Mesa m1 = null;
	private int contadorPareja1;
	private int contadorPareja2;
	private int tipoPares; // tipo 3 = duplex; tipo 2= medias; tipo 1 = pares
	private int tipoJuego; // tipo= 3 tiene31 ; tipo= 2 tieneJuego
	public boolean hayJuego;

	//Los 4 sockets para meter sus OutputStream e InputStream con el fin de poder interaccionar con los jugadores
	private Socket sjugador1 = null;
	private Socket sjugador2 = null;
	private Socket sjugador3 = null;
	private Socket sjugador4 = null;
	
	//Estos se usar�n para los mensajes en com�n
	private PrintWriter pw1 = null;
	private PrintWriter pw2 = null;
	private PrintWriter pw3 = null;
	private PrintWriter pw4 = null;

	public Partida(Socket s1, Socket s2, Socket s3, Socket s4) {
		this.sjugador1 = s1;
		this.sjugador2 = s2;
		this.sjugador3 = s3;
		this.sjugador4 = s4;

		this.tipoPares = 0;
		this.tipoJuego = 0;
		this.lJugadores = new ArrayList<>();
		this.lJugadores.add(new Jugador(1));
		this.lJugadores.add(new Jugador(2));
		this.lJugadores.add(new Jugador(1));
		this.lJugadores.add(new Jugador(2));

		try {
			this.escritores = new ArrayList<>();
			this.escritores.add(this.sjugador1.getOutputStream());
			this.escritores.add(this.sjugador2.getOutputStream());
			this.escritores.add(this.sjugador3.getOutputStream());
			this.escritores.add(this.sjugador4.getOutputStream());
			
			this.lectores = new ArrayList<>();
			this.lectores.add(this.sjugador1.getInputStream());
			this.lectores.add(this.sjugador2.getInputStream());
			this.lectores.add(this.sjugador3.getInputStream());
			this.lectores.add(this.sjugador4.getInputStream());
			
			pw1 = new PrintWriter(this.sjugador1.getOutputStream(), true);
			pw2 = new PrintWriter(this.sjugador2.getOutputStream(), true);
			pw3 = new PrintWriter(this.sjugador3.getOutputStream(), true);
			pw4 = new PrintWriter(this.sjugador4.getOutputStream(), true);

		} catch (IOException e) {
			e.printStackTrace();
		}

		this.m1 = new Mesa();
		this.contadorPareja1 = 0;
		this.contadorPareja2 = 0;
	}

	public void gestionarPartida() {
		//Este metodo repite el ciclo de una partida mientras no hay ganador
		while (!this.hayGanador()) {
			this.iniciarPartida();
			this.ordenarCartasJugadores();
			this.pedrete();
			this.mus();

			this.ordenarCartasJugadores();
			
			int pitasGrande = this.grande();
			this.comparacionGrande(pitasGrande);

			int pitasPequenia = this.pequenia();
			this.comparacionPequenia(pitasPequenia);

			int pitasPares = this.pares();
			this.comparacionPares(pitasPares);

			int pitasJuego = this.juego();

			if (!hayJuego) {
				int pitasPunto = this.punto();
				this.mejorPunto(pitasPunto);
			} else {
				this.comparacionJuego(pitasJuego);
			}
			String s1 = "Informacion: Pareja 1: " + this.contadorPareja1;
			String s2 = "Informacion: Pareja 2: " + this.contadorPareja2;
			pw1.println(s1); pw2.println(s1); pw3.println(s1); pw4.println(s1);
			pw1.println(s2); pw2.println(s2); pw3.println(s2); pw4.println(s2);
			this.eliminarCartasJugadores();
			this.cambiarMano();

		}
		//Envia el caracter especial en caso de que haya finalizado la partida
		pw1.println("$*"); pw2.println("$*"); pw3.println("$*"); pw4.println("$*");

	}

	public void eliminarCartasJugadores() {
		for (Jugador j : this.lJugadores) {
			j.eliminarCartas();
		}
	}
	//Este metodo compara las cartas, para saber que pareja se lleva los puntos de grande, y cuantos hay que sumarles,
	//seg�n como se ha desarrollado la partida
	public void comparacionGrande(int pitas) {
		int j = 0;
		boolean b = false;
		while (j < 4 && b == false) {

			if ((this.lJugadores.get(0).cartas.get(j).getNumero() > this.lJugadores.get(1).cartas.get(j).getNumero()
					&& this.lJugadores.get(0).cartas.get(j).getNumero() > this.lJugadores.get(3).cartas.get(j)
							.getNumero())
					|| this.lJugadores.get(2).cartas.get(j).getNumero() > this.lJugadores.get(1).cartas.get(j)
							.getNumero()
							&& this.lJugadores.get(2).cartas.get(j).getNumero() > this.lJugadores.get(3).cartas.get(j)
									.getNumero()) {
				if (pitas == 0) {
					this.contadorPareja1 += 1;
					b = true;
				} else {
					this.contadorPareja1 += pitas;
					b = true;
				}
			} else {
				if ((this.lJugadores.get(1).cartas.get(j).getNumero() > this.lJugadores.get(0).cartas.get(j).getNumero()
						&& this.lJugadores.get(1).cartas.get(j).getNumero() > this.lJugadores.get(2).cartas.get(j)
								.getNumero())
						|| this.lJugadores.get(3).cartas.get(j).getNumero() > this.lJugadores.get(0).cartas.get(j)
								.getNumero()
								&& this.lJugadores.get(3).cartas.get(j).getNumero() > this.lJugadores.get(2).cartas
										.get(j).getNumero()) {
					if (pitas == 0) {
						this.contadorPareja2 += 1;
						b = true;
					} else {
						this.contadorPareja2 += pitas;
						b = true;
					}
				} else {
					j++;
				}

			}
		}
	}
	//Este metodo compara las cartas, para saber que pareja se lleva los puntos de peque�a, y cuantos hay que sumarles,
	//seg�n como se ha desarrollado la partida
	public void comparacionPequenia(int pitas) {
		int j = 3;
		boolean b = false;
		while (j >= 0 && b == false) {

			if ((this.lJugadores.get(0).cartas.get(j).getNumero() < this.lJugadores.get(1).cartas.get(j).getNumero()
					&& this.lJugadores.get(0).cartas.get(j).getNumero() < this.lJugadores.get(3).cartas.get(j)
							.getNumero())
					|| this.lJugadores.get(2).cartas.get(j).getNumero() < this.lJugadores.get(1).cartas.get(j)
							.getNumero()
							&& this.lJugadores.get(2).cartas.get(j).getNumero() < this.lJugadores.get(3).cartas.get(j)
									.getNumero()) {
				if (pitas == 0) {
					this.contadorPareja1 += 1;
					b = true;
				} else {
					this.contadorPareja1 += pitas;
					b = true;
				}
			} else {
				if ((this.lJugadores.get(1).cartas.get(j).getNumero() < this.lJugadores.get(0).cartas.get(j).getNumero()
						&& this.lJugadores.get(1).cartas.get(j).getNumero() < this.lJugadores.get(2).cartas.get(j)
								.getNumero())
						|| this.lJugadores.get(3).cartas.get(j).getNumero() < this.lJugadores.get(0).cartas.get(j)
								.getNumero()
								&& this.lJugadores.get(3).cartas.get(j).getNumero() < this.lJugadores.get(2).cartas
										.get(j).getNumero()) {
					if (pitas == 0) {
						this.contadorPareja2 += 1;
						b = true;
					} else {
						this.contadorPareja2 += pitas;
						b = true;
					}
				} else {
					j--;
				}

			}
		}
	}
	//Compara entre dos jugadores, y devulve el jugador que mejor pares tenga. Puede devolver null si ninguno tiene pares
	public Jugador mejoresPares(Jugador j1, Jugador j2) {
		if (j1 == null && j2 == null) {
			return null;
		} else {
			if (j1 == null) {
				return j2;
			} else {
				if (j2 == null) {
					return j1;
				} else {
					if (j1.tieneDuplex() && !j2.tieneDuplex()) {
						this.tipoPares = 3;
						return j1;

					} else {
						if (!j1.tieneDuplex() && j2.tieneDuplex()) {
							this.tipoPares = 3;
							return j2;

						} else {
							if (j1.tieneDuplex() && j2.tieneDuplex()) {
								if (j1.cartas.get(0).getNumero() > j2.cartas.get(0).getNumero()) {
									this.tipoPares = 3;
									return j1;

								} else {
									if (j1.cartas.get(0).getNumero() < j2.cartas.get(0).getNumero()) {
										this.tipoPares = 3;
										return j2;
									} else {
										if (j1.cartas.get(2).getNumero() > j2.cartas.get(2).getNumero()) {
											this.tipoPares = 3;
											return j1;
										} else {
											if (j1.cartas.get(2).getNumero() < j2.cartas.get(2).getNumero()) {
												this.tipoPares = 3;
												return j2;
											} else {
												this.tipoPares = 3;
												return j1; // EN CASO DE QUE QUE SEAN IGUALES GANA LA MANO
											}
										}
									}
								}
							}

							else {
								if (j1.tieneMedias() && !j2.tieneMedias()) {
									this.tipoPares = 2;
									return j1;
								} else {
									if (!j1.tieneMedias() && j2.tieneMedias()) {
										this.tipoPares = 2;
										return j2;
									}

									else {
										if (j1.cartas.get(0).getNumero() == j1.cartas.get(1).getNumero()) {
											int numMedJ1 = j1.cartas.get(0).getNumero();
											int numMedJ2;

											if (j2.cartas.get(0).getNumero() == j2.cartas.get(1).getNumero()) {
												numMedJ2 = j2.cartas.get(0).getNumero();
											} else {
												numMedJ2 = j2.cartas.get(1).getNumero();
											}
											if (numMedJ1 > numMedJ2) {
												this.tipoPares = 2;
												return j1;
											} else {

												return j2;
											}
										}
										if (j1.cartas.get(0).getNumero() != j1.cartas.get(1).getNumero()) {
											int numMedJ1 = j1.cartas.get(1).getNumero();
											int numMedJ2;

											if (j2.cartas.get(0).getNumero() == j2.cartas.get(1).getNumero()) {
												numMedJ2 = j2.cartas.get(0).getNumero();
											} else {
												numMedJ2 = j2.cartas.get(1).getNumero();
											}
											if (numMedJ1 > numMedJ2) {
												this.tipoPares = 2;
												return j1;
											} else {
												this.tipoPares = 2;
												return j2;
											}
										} else {
											if (j1.tienePares() && !j2.tienePares()) {
												this.tipoPares = 1;
												return j1;
											} else {
												if (!j1.tienePares() && j2.tienePares()) {
													this.tipoPares = 1;
													return j2;
												} else {
													if (!j1.tienePares() && !j2.tienePares()) {
														return null;
													} else {
														if (j1.cartas.get(0).getNumero() == j1.cartas.get(1)
																.getNumero()) {
															int numMedJ1 = j1.cartas.get(0).getNumero();
															int numMedJ2;

															if (j2.cartas.get(0).getNumero() == j2.cartas.get(1)
																	.getNumero()) {
																numMedJ2 = j2.cartas.get(0).getNumero();
															} else {
																if (j2.cartas.get(1).getNumero() == j2.cartas.get(2)
																		.getNumero()) {
																	numMedJ2 = j2.cartas.get(1).getNumero();
																} else {
																	numMedJ2 = j2.cartas.get(2).getNumero();
																}

															}
															if (numMedJ1 > numMedJ2) {
																this.tipoPares = 1;
																return j1;
															} else {
																this.tipoPares = 1;
																return j2;
															}
														} else {
															if (j1.cartas.get(1).getNumero() == j1.cartas.get(2)
																	.getNumero()) {
																int numMedJ1 = j1.cartas.get(1).getNumero();
																int numMedJ2;

																if (j2.cartas.get(0).getNumero() == j2.cartas.get(1)
																		.getNumero()) {
																	numMedJ2 = j2.cartas.get(0).getNumero();
																} else {
																	if (j2.cartas.get(1).getNumero() == j2.cartas.get(2)
																			.getNumero()) {
																		numMedJ2 = j2.cartas.get(1).getNumero();
																	} else {
																		numMedJ2 = j2.cartas.get(2).getNumero();
																	}

																}
																if (numMedJ1 > numMedJ2) {
																	this.tipoPares = 1;
																	return j1;
																} else {
																	this.tipoPares = 1;
																	return j2;
																}
															} else {
																int numMedJ1 = j1.cartas.get(2).getNumero();
																int numMedJ2;

																if (j2.cartas.get(0).getNumero() == j2.cartas.get(1)
																		.getNumero()) {
																	numMedJ2 = j2.cartas.get(0).getNumero();
																} else {
																	if (j2.cartas.get(1).getNumero() == j2.cartas.get(2)
																			.getNumero()) {
																		numMedJ2 = j2.cartas.get(1).getNumero();
																	} else {
																		numMedJ2 = j2.cartas.get(2).getNumero();
																	}

																}
																if (numMedJ1 > numMedJ2) {
																	this.tipoPares = 1;
																	return j1;
																} else {
																	this.tipoPares = 1;
																	return j2;
																}
															}
														}

													}
												}

											}
										}

									}
								}
							}
						}

					}
				}
			}
		}

	}

	public void comparacionPares(int pitas) { // Solamente sumamos al contador los pares m�s altos de uno de la pareja.
												// En caso de tener los dos pares solo se tendr�n en cuenta los m�s
												// altos.
		Jugador pareja1 = this.mejoresPares(lJugadores.get(0), lJugadores.get(2));
		Jugador pareja2 = this.mejoresPares(lJugadores.get(1), lJugadores.get(3));
		Jugador ganador = this.mejoresPares(pareja1, pareja2);
		if (ganador != null) {
			if (ganador.getPareja() == 1) {
				contadorPareja1 += this.tipoPares + pitas;
			} else {
				contadorPareja2 += this.tipoPares + pitas;
			}
		}

	}
	//Compara entre dos jugadores, y devulve el jugador que mejor pares tenga. Puede devolver null si ninguno tiene pares
	public Jugador mejorJuego(Jugador j1, Jugador j2) {
		if (j1 == null && j2 == null) {
			return null;
		} else {
			if (j1 == null) {
				return j2;
			} else {
				if (j2 == null) {
					return j1;
				} else {
					if (j1.tiene31() && !j2.tiene31()) {
						this.tipoJuego = 3;
						return j1;
					} else {
						if (j2.tiene31() && !j1.tiene31()) {
							this.tipoJuego = 3;
							return j2;
						} else {
							if (j1.tiene31() && j2.tiene31()) {
								this.tipoJuego = 3;
								return j1;
							} else {
								if (j1.getJuego() == 32 && j2.getJuego() != 32) {
									this.tipoJuego = 2;
									return j1;
								} else {
									if (j1.getJuego() != 32 && j2.getJuego() == 32) {
										this.tipoJuego = 2;
										return j2;
									} else {
										if (j1.getJuego() == 32 && j2.getJuego() == 32) {
											this.tipoJuego = 2;
											return j1;
										} else {
											if (j1.getJuego() > j2.getJuego()) {
												this.tipoJuego = 2;
												return j1;
											} else {
												if (j1.getJuego() < j2.getJuego()) {
													this.tipoJuego = 2;
													return j2;
												} else {
													if (j1.getJuego() == 0 && j2.getJuego() == 0) {

														return null;
													}

												}
											}
										}
									}
								}

							}

						}

					}
				}
			}
		}

		this.tipoJuego = 2;
		return j1;
	}
	//Suma las pitas a la pareja cuyo jugador tiene mejor juego
	public void comparacionJuego(int pitas) {
		Jugador pareja1 = this.mejorJuego(lJugadores.get(0), lJugadores.get(2));
		Jugador pareja2 = this.mejorJuego(lJugadores.get(1), lJugadores.get(3));
		Jugador ganador = this.mejorJuego(pareja1, pareja2);
		if (ganador != null) {
			if (ganador.getPareja() == 1) {
				contadorPareja1 += this.tipoJuego + pitas;
			} else {
				contadorPareja2 += this.tipoJuego + pitas;
			}
		}
	}
	//Suma las pitas a la pareja cuyo jugador tenga mejor punto
	public void mejorPunto(int pitas) {
		Jugador ganador = lJugadores.get(0);
		int puntoMayor;
		puntoMayor = lJugadores.get(0).getPunto();
		for (int i = 0; i < 4; i++) {

			if (puntoMayor < lJugadores.get(i).getPunto()) {
				puntoMayor = lJugadores.get(i).getPunto();
				ganador = lJugadores.get(i);
			}

		}
		if (ganador.getPareja() == 1) {
			this.contadorPareja1 += 1 + pitas;
		} else {
			this.contadorPareja2 += 1 + pitas;
		}
	}
	//Repearte las cartas a todos los jugadores
	public void iniciarPartida() {
		this.m1 = new Mesa();
		for (Jugador j : this.lJugadores) {
			j.obtenerCartasInicial(m1);
		}
	}
	//Devulve true en el caso de que el contador de alguna de las parejas, sea mayor o igual de 25 y false caso contrario
	public boolean hayGanador() {

		if (contadorPareja1 >= 25) {
			pw1.println("Informacion: ����LOS GANADORES SON LA PAREJA UNO!!!!!"); 
			pw2.println("Informacion: ����LOS GANADORES SON LA PAREJA UNO!!!!!"); 
			pw3.println("Informacion: ����LOS GANADORES SON LA PAREJA UNO!!!!!"); 
			pw4.println("Informacion: ����LOS GANADORES SON LA PAREJA UNO!!!!!"); 
			return true;
		}
		if (contadorPareja2 >= 25) {
			pw1.println("Informacion: ����LOS GANADORES SON LA PAREJA DOS!!!!!"); 
			pw2.println("Informacion: ����LOS GANADORES SON LA PAREJA DOS!!!!!"); 
			pw3.println("Informacion: ����LOS GANADORES SON LA PAREJA DOS!!!!!"); 
			pw4.println("Informacion: ����LOS GANADORES SON LA PAREJA DOS!!!!!"); 
			return true;
		}
		return false;
	}
	//Reparte otras 4 cartas a todos los jugadores, debido a que algun jugador tenia pedrete
	public void pedrete() {
		for (Jugador j : this.lJugadores) {
			if (j.tienePedrete()) {

				for (int i = 0; i < 4; i++) {
					Carta c = j.cartas.remove(i);
					this.m1.a�adirCartaMontonMus(c);
					if (!this.m1.montonVacio()) {
						j.cartas.add(i, this.m1.getCartaMonton());
					} else {
						j.cartas.add(i, this.m1.getCartaMus());
					}

				}
			}
		}
	}
	//Pregunta a los jugadores si quieren descartarse, y cuantas cartas se quieren quitar, y en el caso de que
	//todos quieran, da la opcion al jugador de elegir que cartas quiere quitarse
	public void mus() {
		boolean b = true;
		while (b) {

			// Comprobamos si quiere mus ( 1-4) o no quieren (0)
			int[] numCartas = new int[4];
			int n = 0;
			PrintWriter pw = null;
			DataInputStream dis = null;
			String s = null;
			for (Jugador j : this.lJugadores) {
				pw = new PrintWriter(this.escritores.get(n), true);
				pw.println(j.mostrarCartas() + "�Quieres mus?[y/n]");
				dis = new DataInputStream(this.lectores.get(n));
				try {
					s = dis.readLine();
				} catch (IOException e) {
					e.printStackTrace();
				}
				if(s.equals("y")) {
					pw.println("�Cuantas cartas quieres quitarte?");
					try {
						int i = Integer.parseInt(dis.readLine());
						numCartas[n] = i;
					} catch (NumberFormatException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				else {
					numCartas[n] = 0;
				}
				n++;
			}
			int i = 0;
			while (i < 4 && b == true) {
				if (numCartas[i] == 0) {
					b = false;
				}
				i++;
			}
			// Si hay mus
			if (b) {
				int j2 = 0;
				for (Jugador j : this.lJugadores) {
					pw = new PrintWriter(this.escritores.get(j2), true);
					dis = new DataInputStream(this.lectores.get(j2));
					for (int i2 = 0; i2 < numCartas[j2]; i2++) {
						int posicion = -1;
						pw.println("Jugador: " + j2+ " �Qu� cartas quieres quitarte? [0-3] (De mas a menos posicion)");
						try {
							posicion = Integer.parseInt(dis.readLine());
						} catch (NumberFormatException e) {
							e.printStackTrace();
						} catch (IOException e) {
							e.printStackTrace();
						}
						Carta c = j.cartas.remove(posicion);
						this.m1.a�adirCartaMontonMus(c);
						if (!this.m1.montonVacio()) {
							j.cartas.add(posicion, this.m1.getCartaMonton());
						} else {
							j.cartas.add(posicion, this.m1.getCartaMus());
						}
					}
					j2++;
				}
			}
		}

	}
	//Cambia de orden al juagador para que no vaya de mano siempre el primero
	public void cambiarMano() {
		Jugador j1 = this.lJugadores.get(0);
		this.lJugadores.remove(j1);
		this.lJugadores.add(j1);
		
		InputStream is = this.lectores.get(0);
		this.lectores.remove(is);
		this.lectores.add(is);
		
		OutputStream os = this.escritores.get(0);
		this.escritores.remove(os);
		this.escritores.add(os);
	}

	public void ordenarCartasJugadores() {
		for (Jugador j : this.lJugadores) {
			j.ordenarCartas();
		}
	}
	//Juega la grande y devulve el numero de pitasEnvidadas en la partida, que puede ser 0 en caso de que
	//todos los jugadores pasen, no enviden...
	public int grande() {
		Jugador j = null;
		int pitasEnvidadas = 0;
		boolean finalizado = false;
		int jugadorActual = 0;
		boolean primerEnvite = true;
		int e = 0;
		int envMas = 0;
		String s = "Informacion: GRANDE";
		pw1.println(s); pw2.println(s); pw3.println(s); pw4.println(s); 

		while (!finalizado && pitasEnvidadas < 25 && jugadorActual < 4) {
			PrintWriter pw = null;
			DataInputStream dis = null;
			String respuesta = "";

			j = this.lJugadores.get(jugadorActual);

			pw = new PrintWriter(this.escritores.get(jugadorActual), true);

			pw.println("El jugador " + jugadorActual + " habla [1: paso, 2: envido, 3: quiero, 4: no quiero] "
					+ j.mostrarCartas());

			dis = new DataInputStream(this.lectores.get(jugadorActual));

			try {
				respuesta = dis.readLine();
			} catch (IOException e1) {
				e1.printStackTrace();
			}

			int n = Integer.parseInt(respuesta);
			if (n == 1) {
				String s1 = "Informacion: El jugador "+jugadorActual +" ha pasado";
				pw1.println(s1); pw2.println(s1); pw3.println(s1); pw4.println(s1);
				jugadorActual++;
			}
			if (n == 2) {
				pw.println("�Cuanto envidas?");

				if (primerEnvite) {
					try {
						e = Integer.parseInt(dis.readLine());
					} catch (NumberFormatException e1) {
						e1.printStackTrace();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
					primerEnvite = false;
				} else {
					try {
						envMas = Integer.parseInt(dis.readLine());
					} catch (NumberFormatException e1) {
						e1.printStackTrace();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}

				if (e < 25) {
					if (envMas == 0) {
						String s2  = "Informacion: El jugador "+jugadorActual +" envida " + e;
						pw1.println(s2); pw2.println(s2); pw3.println(s2); pw4.println(s2); 
					} else {
						String s2  = "Informacion: El jugador "+jugadorActual +" envida " + envMas;
						pw1.println(s2); pw2.println(s2); pw3.println(s2); pw4.println(s2); 
					}

					if (jugadorActual == 3) {
						jugadorActual = 0;
					} else {
						jugadorActual++;
					}

				} else {

				}
			}
			if (n == 3) {
				e += envMas;
				pitasEnvidadas = e;
				finalizado = true;
				String s3 = "Informacion: El jugador " + jugadorActual + " ha querido";
				pw1.println(s3); pw2.println(s3); pw3.println(s3); pw3.println(s3); 
			}
			if (n == 4) {

				String s4 = "Informacion: El jugador " + jugadorActual +" no ha querido";
				pw1.println(s4); pw2.println(s4); pw3.println(s4); pw4.println(s4); 
				if (jugadorActual == 0 || jugadorActual == 1) {
					jugadorActual += 2;
					j = this.lJugadores.get(jugadorActual);
					pw = new PrintWriter(this.escritores.get(jugadorActual), true);
					pw.println("El jugador " + jugadorActual + " habla [ 1: envido m�s, 2: quiero, 3: no quiero] "
							+ j.mostrarCartas());
					dis = new DataInputStream(this.lectores.get(jugadorActual));
					int n4 = 0;
					try {
						n4 = Integer.parseInt(dis.readLine());
					} catch (NumberFormatException e1) {
						e1.printStackTrace();
					} catch (IOException e1) {
						e1.printStackTrace();
					}

					if (n4 == 1) {
						primerEnvite = false;
						pw.println("�Cuanto m�s quieres envidar?");
						try {
							envMas = Integer.parseInt(dis.readLine());
						} catch (NumberFormatException e1) {
							e1.printStackTrace();
						} catch (IOException e1) {
							e1.printStackTrace();
						}
						e += envMas;
						String s5 = "Informacion: El jugador " + jugadorActual + " ha envidado mas "+envMas;
						pw1.println(s5); pw2.println(s5); pw3.println(s5); pw4.println(s5);
						if (jugadorActual == 3) {
							jugadorActual = 0;
						} else {
							jugadorActual++;
						}

					}
					if (n4 == 2) {

						String s5 = "Informacion: El jugador " + jugadorActual + " ha querido";
						pw1.println(s5); pw2.println(s5); pw3.println(s5); pw4.println(s5);
						finalizado = true;
					}
					if (n4 == 3) {
						if (primerEnvite) {
							pitasEnvidadas = 1;
						} else {
							pitasEnvidadas = e;
						}
						String s5 = "Informacion: El jugador " + jugadorActual + " no ha querido";
						pw1.println(s5); pw2.println(s5); pw3.println(s5); pw4.println(s5);
						finalizado = true;
					}

				} else {
					jugadorActual -= 2;
					j = this.lJugadores.get(jugadorActual);
					pw = new PrintWriter(this.escritores.get(jugadorActual), true);
					pw.println("El jugador " + jugadorActual + " habla [ 1: envido m�s, 2: quiero, 3: no quiero] "
							+ j.mostrarCartas());
					dis = new DataInputStream(this.lectores.get(jugadorActual));
					int n4 = -1;
					try {
						n4 = Integer.parseInt(dis.readLine());
					} catch (NumberFormatException e1) {
						e1.printStackTrace();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
					if (n4 == 1) {
						primerEnvite = false;
						pw.println("�Cuanto m�s quieres envidar?");
						try {
							envMas = Integer.parseInt(dis.readLine());
						} catch (NumberFormatException e1) {
							e1.printStackTrace();
						} catch (IOException e1) {
							e1.printStackTrace();
						}
						e += envMas;
						String s5 = "Informacion: El jugador " + jugadorActual + " ha envidado mas "+envMas;
						pw1.println(s5); pw2.println(s5); pw3.println(s5); pw4.println(s5);
						if (jugadorActual == 3) {
							jugadorActual = 0;
						} else {
							jugadorActual++;
						}
					}
					if (n4 == 2) {
						String s5 = "Informacion: El jugador " + jugadorActual + " ha querido";
						pw1.println(s5); pw2.println(s5); pw3.println(s5); pw4.println(s5);
						finalizado = true;
					}
					if (n4 == 3) {
						if (primerEnvite) {
							pitasEnvidadas = 1;
						} else {
							pitasEnvidadas = e;
						}
						String s5 = "Informacion: El jugador " + jugadorActual + " no ha querido";
						pw1.println(s5); pw2.println(s5); pw3.println(s5); pw4.println(s5);
						finalizado = true;
					}
				}
			}
		}
		return pitasEnvidadas;
	}

	//Juega la peque�a y devulve el numero de pitasEnvidadas en la partida, que puede ser 0 en caso de que
	//todos los jugadores pasen, no enviden...
	public int pequenia() {
		Jugador j = null;
		int pitasEnvidadas = 0;
		boolean finalizado = false;
		int jugadorActual = 0;
		boolean primerEnvite = true;
		int e = 0;
		int envMas = 0;
		String s = "Informacion: PEQUENIA";
		PrintWriter pw = null;
		DataInputStream dis = null;
		pw1.println(s); pw2.println(s); pw3.println(s); pw4.println(s); 

		while (!finalizado && pitasEnvidadas < 25 && jugadorActual < 4) {
			j = this.lJugadores.get(jugadorActual);
			pw = new PrintWriter(this.escritores.get(jugadorActual), true);
			pw.println("El jugador " + jugadorActual + " habla [1: paso, 2: envido, 3: quiero, 4: no quiero] "
					+ j.mostrarCartas());
			dis = new DataInputStream(this.lectores.get(jugadorActual));

			int n = 0;
			try {
				n = Integer.parseInt(dis.readLine());
			} catch (NumberFormatException e1) {
				e1.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			if (n == 1) {
				String s1 = "Informacion: El jugador "+jugadorActual+" ha pasado";
				pw1.println(s1); pw2.println(s1); pw3.println(s1); pw4.println(s1); 
				jugadorActual++;
			}
			if (n == 2) {
				pw.println("�Cuanto envidas?");

				if (primerEnvite) {
					try {
						e = Integer.parseInt(dis.readLine());
					} catch (NumberFormatException e1) {
						e1.printStackTrace();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
					primerEnvite = false;
				} else {
					try {
						envMas = Integer.parseInt(dis.readLine());
					} catch (NumberFormatException e1) {
						e1.printStackTrace();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}

				if (e < 25) {

					if (envMas == 0) {
						String s2  = "Informacion: El jugador "+jugadorActual +" envida " + e;
						pw1.println(s2); pw2.println(s2); pw3.println(s2); pw4.println(s2); 
					} else {
						String s2  = "Informacion: El jugador "+jugadorActual +" envida " + envMas;
						pw1.println(s2); pw2.println(s2); pw3.println(s2); pw4.println(s2); 
					}

					if (jugadorActual == 3) {
						jugadorActual = 0;
					} else {
						jugadorActual++;
					}
				} else {

				}
			}
			if (n == 3) {
				String s2  = "Informacion: El jugador "+jugadorActual +" ha querido";
				pw1.println(s2); pw2.println(s2); pw3.println(s2); pw4.println(s2); 
				e += envMas;
				pitasEnvidadas = e;
				finalizado = true;
			}
			if (n == 4) {
				String s4 = "Informacion: El jugador " + jugadorActual +" no ha querido";
				pw1.println(s4); pw2.println(s4); pw3.println(s4); pw4.println(s4); 
				if (jugadorActual == 0 || jugadorActual == 1) {
					jugadorActual += 2;
					j = this.lJugadores.get(jugadorActual);
					pw = new PrintWriter(this.escritores.get(jugadorActual), true);
					pw.println("El jugador " + jugadorActual + " habla [ 1: envido m�s, 2: quiero, 3: no quiero] "
							+ j.mostrarCartas());
					dis = new DataInputStream(this.lectores.get(jugadorActual));
					int n4 = 0;
					try {
						n4 = Integer.parseInt(dis.readLine());
					} catch (NumberFormatException e1) {
						e1.printStackTrace();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
					if (n4 == 1) {
						primerEnvite = false;
						pw.println("�Cuanto m�s quieres envidar?");
						try {
							envMas = Integer.parseInt(dis.readLine());
						} catch (NumberFormatException e1) {
							e1.printStackTrace();
						} catch (IOException e1) {
							e1.printStackTrace();
						}
						e += envMas;
						String s2  = "Informacion: El jugador "+jugadorActual +" ha envidado mas " + envMas;
						pw1.println(s2); pw2.println(s2); pw3.println(s2); pw4.println(s2); 
						if (jugadorActual == 3) {
							jugadorActual = 0;
						} else {
							jugadorActual++;
						}

					}
					if (n4 == 2) {
						String s2  = "Informacion: El jugador "+jugadorActual +" ha envidado querido";
						pw1.println(s2); pw2.println(s2); pw3.println(s2); pw4.println(s2); 
						finalizado = true;
					}
					if (n4 == 3) {
						if (primerEnvite) {
							pitasEnvidadas = 1;
						} else {
							pitasEnvidadas = e;
						}
						String s2  = "Informacion: El jugador "+jugadorActual +" no ha querido";
						pw1.println(s2); pw2.println(s2); pw3.println(s2); pw4.println(s2); 
						finalizado = true;
					}

				} else {
					jugadorActual -= 2;
					j = this.lJugadores.get(jugadorActual);
					pw = new PrintWriter(this.escritores.get(jugadorActual), true);
					pw.println("El jugador " + jugadorActual + " habla [ 1: envido m�s, 2: quiero, 3: no quiero] "
							+ j.mostrarCartas());
					dis = new DataInputStream(this.lectores.get(jugadorActual));
					int n4 = 0;
					try {
						n4 = Integer.parseInt(dis.readLine());
					} catch (NumberFormatException e1) {
						e1.printStackTrace();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
					if (n4 == 1) {
						primerEnvite = false;
						pw.println("�Cuanto m�s quieres envidar?");
						try {
							envMas = Integer.parseInt(dis.readLine());
						} catch (NumberFormatException e1) {
							e1.printStackTrace();
						} catch (IOException e1) {
							e1.printStackTrace();
						}
						e += envMas;
						String s2  = "Informacion: El jugador "+jugadorActual +" ha envidado mas " + envMas;
						pw1.println(s2); pw2.println(s2); pw3.println(s2); pw4.println(s2); 
						if (jugadorActual == 3) {
							jugadorActual = 0;
						} else {
							jugadorActual++;
						}

					}
					if (n4 == 2) {
						String s2  = "Informacion: El jugador "+jugadorActual +" ha querido";
						pw1.println(s2); pw2.println(s2); pw3.println(s2); pw4.println(s2); 
						finalizado = true;
					}
					if (n4 == 3) {
						if (primerEnvite) {
							pitasEnvidadas = 1;
						} else {
							pitasEnvidadas = e;
						}
						String s2  = "Informacion: El jugador "+jugadorActual +" no ha querido";
						pw1.println(s2); pw2.println(s2); pw3.println(s2); pw4.println(s2); 
						finalizado = true;
					}
				}
			}
		}
		return pitasEnvidadas;
	}
	//Juega los pares en el caso de que alguien de las 2 parejas tenga, y devulve el numero de pitasEnvidadas en la partida, que puede ser 0 en caso de que
	//todos los jugadores pasen, no enviden o tambien, porque no se haya jugado
	public int pares() {
		List<Jugador> puedenJugar = new ArrayList<>();
		for (Jugador j : this.lJugadores) {
			if (j.tienePares()) {
				puedenJugar.add(j);
			}
		}
		if (puedenJugar.isEmpty() || puedenJugar.size() == 1
				|| (this.lJugadores.get(0).tienePares() && this.lJugadores.get(2).tienePares()
						&& !this.lJugadores.get(1).tienePares() && !this.lJugadores.get(3).tienePares())
				|| (this.lJugadores.get(1).tienePares() && this.lJugadores.get(3).tienePares()
						&& !this.lJugadores.get(0).tienePares() && !this.lJugadores.get(2).tienePares())) {
			return 0;
		} else {

			Jugador j = null;
			int pitasEnvidadas = 0;
			boolean finalizado = false;
			int jugadorActual = 0;
			boolean primerEnvite = true;
			int e = 0;
			int envMas = 0;
			PrintWriter pw = null;
			DataInputStream dis = null;

			String s = "Informacion: PARES";
			pw1.println(s); pw2.println(s); pw3.println(s); pw4.println(s); 

			while (!finalizado && pitasEnvidadas < 25 && jugadorActual < 4) {
				j = this.lJugadores.get(jugadorActual);
				if (j.tienePares()) {
					pw = new PrintWriter(this.escritores.get(jugadorActual), true);
					pw.println("El jugador " + jugadorActual + " habla [1: paso, 2: envido, 3: quiero, 4: no quiero] "
							+ j.mostrarCartas());
					dis = new DataInputStream(this.lectores.get(jugadorActual));

					int n = 0;
					try {
						n = Integer.parseInt(dis.readLine());
					} catch (NumberFormatException e1) {
						e1.printStackTrace();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
					if (n == 1) {
						String s1 = "Informacion: El jugador "+jugadorActual+" ha pasado";
						pw1.println(s1); pw2.println(s1); pw3.println(s1); pw4.println(s1);
						jugadorActual++;
					}
					if (n == 2) {
						pw.println("�Cuanto envidas?");

						if (primerEnvite) {
							try {
								e = Integer.parseInt(dis.readLine());
							} catch (NumberFormatException e1) {
								e1.printStackTrace();
							} catch (IOException e1) {
								e1.printStackTrace();
							}
							primerEnvite = false;
						} else {
							try {
								envMas = Integer.parseInt(dis.readLine());
							} catch (NumberFormatException e1) {
								e1.printStackTrace();
							} catch (IOException e1) {
								e1.printStackTrace();
							}
						}

						if (e < 25) {

							if (envMas == 0) {
								String s1 = "Informacion: El jugador "+jugadorActual+" ha envidado "+e;
								pw1.println(s1); pw2.println(s1); pw3.println(s1); pw4.println(s1);
							} else {
								String s1 = "Informacion: El jugador "+jugadorActual+" ha envidado "+envMas;
								pw1.println(s1); pw2.println(s1); pw3.println(s1); pw4.println(s1);
							}
							if (jugadorActual == 3) {
								jugadorActual = 0;
							} else {
								jugadorActual++;
							}
						} else {

						}
					}
					if (n == 3) {
						String s1 = "Informacion: El jugador "+jugadorActual+" ha querido";
						pw1.println(s1); pw2.println(s1); pw3.println(s1); pw4.println(s1);
						e += envMas;
						pitasEnvidadas = e;
						finalizado = true;
					}
					if (n == 4) {
						String s1 = "Informacion: El jugador "+jugadorActual+" no ha querido";
						pw1.println(s1); pw2.println(s1); pw3.println(s1); pw4.println(s1);
						if (jugadorActual == 0 || jugadorActual == 1) {
							jugadorActual += 2;
							j = this.lJugadores.get(jugadorActual);
							if (j.tienePares()) {
								pw = new PrintWriter(this.escritores.get(jugadorActual),true);
								pw.println("El jugador " + jugadorActual
										+ "habla [ 1: envido m�s, 2: quiero, 3: no quiero] " + j.mostrarCartas());
								dis = new DataInputStream(this.lectores.get(jugadorActual));
								int n4 = 0;
								try {
									n4 = Integer.parseInt(dis.readLine());
								} catch (NumberFormatException e1) {
									e1.printStackTrace();
								} catch (IOException e1) {
									e1.printStackTrace();
								}

								if (n4 == 1) {
									primerEnvite = false;
									pw.println("�Cuanto m�s quieres envidar?");
									try {
										envMas = Integer.parseInt(dis.readLine());
									} catch (NumberFormatException e1) {
										e1.printStackTrace();
									} catch (IOException e1) {
										e1.printStackTrace();
									}
									e += envMas;
									String s2 = "Informacion: El jugador "+jugadorActual+" ha envidado mas "+envMas;
									pw1.println(s2); pw2.println(s2); pw3.println(s2); pw4.println(s2);
									if (jugadorActual == 3) {
										jugadorActual = 0;
									} else {
										jugadorActual++;
									}

								}
								if (n4 == 2) {
									String s2 = "Informacion: El jugador "+jugadorActual+" ha querido";
									pw1.println(s2); pw2.println(s2); pw3.println(s2); pw4.println(s2);
									finalizado = true;
								}
								if (n4 == 3) {
									if (primerEnvite) {
										pitasEnvidadas = 1;
									} else {
										pitasEnvidadas = e;
									}
									String s2 = "Informacion: El jugador "+jugadorActual+" no ha querido";
									pw1.println(s2); pw2.println(s2); pw3.println(s2); pw4.println(s2);
									finalizado = true;
								}
							} else {
								if (jugadorActual == 3) {
									jugadorActual = 0;
								} else {
									jugadorActual++;
								}
							}

						} else {
							jugadorActual -= 2;
							j = this.lJugadores.get(jugadorActual);
							if (j.tienePares()) {
								pw = new PrintWriter(this.escritores.get(jugadorActual),true);
								pw.println("El jugador " + jugadorActual
										+ "habla [ 1: envido m�s, 2: quiero, 3: no quiero] " + j.mostrarCartas());
								dis = new DataInputStream(this.lectores.get(jugadorActual));
								int n4 = 0;
								try {
									n4 = Integer.parseInt(dis.readLine());
								} catch (NumberFormatException e1) {
									e1.printStackTrace();
								} catch (IOException e1) {
									e1.printStackTrace();
								}

								if (n4 == 1) {
									primerEnvite = false;
									pw.println("�Cuanto m�s quieres envidar?");
									try {
										envMas = Integer.parseInt(dis.readLine());
									} catch (NumberFormatException e1) {
										e1.printStackTrace();
									} catch (IOException e1) {
										e1.printStackTrace();
									}

									e += envMas;
									String s2 = "Informacion: El jugador "+jugadorActual+" ha envidado mas "+envMas;
									pw1.println(s2); pw2.println(s2); pw3.println(s2); pw4.println(s2);
									if (jugadorActual == 3) {
										jugadorActual = 0;
									} else {
										jugadorActual++;
									}

								}
								if (n4 == 2) {
									String s2 = "Informacion: El jugador "+jugadorActual+" ha querido";
									pw1.println(s2); pw2.println(s2); pw3.println(s2); pw4.println(s2);
									finalizado = true;
								}
								if (n4 == 3) {
									if (primerEnvite) {
										pitasEnvidadas = 1;
									} else {
										pitasEnvidadas = e;
									}
									String s2 = "Informacion: El jugador "+jugadorActual+" no ha querido";
									pw1.println(s2); pw2.println(s2); pw3.println(s2); pw4.println(s2);
									finalizado = true;
								}
							} else {
								if (jugadorActual == 3) {
									jugadorActual = 0;
								} else {
									jugadorActual++;
								}
							}

						}
					}
				} else {
					jugadorActual++;
				}

			}
			return pitasEnvidadas;

		}

	}
	//Juega el juego en el caso de que alguien de las 2 parejas tenga, y devulve el numero de pitasEnvidadas en la partida, que puede ser 0 en caso de que
	//todos los jugadores pasen, no enviden o tambien, porque no se haya jugado
	public int juego() {
		List<Jugador> puedenJugar = new ArrayList<>();
		for (Jugador j : this.lJugadores) {
			if (j.tieneJuego()) {
				puedenJugar.add(j);
			}
		}
		if (puedenJugar.isEmpty()) {
			this.hayJuego = false;
			return 0;
		} else {
			if (puedenJugar.size() == 1
					|| (this.lJugadores.get(0).tieneJuego() && this.lJugadores.get(2).tieneJuego()
							&& !this.lJugadores.get(1).tieneJuego() && !this.lJugadores.get(3).tieneJuego())
					|| (this.lJugadores.get(1).tieneJuego() && this.lJugadores.get(3).tieneJuego()
							&& !this.lJugadores.get(0).tieneJuego() && !this.lJugadores.get(2).tieneJuego())) {
				this.hayJuego = true;
				return 0;
			} else {
				this.hayJuego = true;
				Jugador j = null;
				int pitasEnvidadas = 0;
				boolean finalizado = false;
				int jugadorActual = 0;
				boolean primerEnvite = true;
				int e = 0;
				int envMas = 0;
				String s = "Informacion: JUEGO";
				pw1.println(s); pw2.println(s); pw3.println(s); pw4.println(s); 
				PrintWriter pw = null;
				DataInputStream dis = null;

				while (!finalizado && pitasEnvidadas < 25 && jugadorActual < 4) {

					j = this.lJugadores.get(jugadorActual);
					if (j.tieneJuego()) {
						pw = new PrintWriter(this.escritores.get(jugadorActual),true);
						pw.println("El jugador " + jugadorActual + " habla [1: paso, 2: envido, 3: quiero, 4: no quiero] " + j.mostrarCartas());
						dis = new DataInputStream(this.lectores.get(jugadorActual));
						int n = 0;
						try {
							n = Integer.parseInt(dis.readLine());
						} catch (NumberFormatException e1) {
							e1.printStackTrace();
						} catch (IOException e1) {
							e1.printStackTrace();
						}
						if (n == 1) {
							String s1 = "Informacion: El jugador "+jugadorActual+" ha pasado";
							pw1.println(s1); pw2.println(s1); pw3.println(s1); pw4.println(s1);
							jugadorActual++;
						}
						if (n == 2) {
							pw.println("�Cuanto envidas?");

							if (primerEnvite) {
								try {
									e = Integer.parseInt(dis.readLine());
								} catch (NumberFormatException e1) {
									e1.printStackTrace();
								} catch (IOException e1) {
									e1.printStackTrace();
								}
								primerEnvite = false;
							} else {
								try {
									envMas = Integer.parseInt(dis.readLine());
								} catch (NumberFormatException e1) {
									e1.printStackTrace();
								} catch (IOException e1) {
									e1.printStackTrace();
								}
							}
							if (e < 25) {

								if (envMas == 0) {
									String s1 = "Informacion: El jugador "+jugadorActual+" ha envidado "+e;
									pw1.println(s1); pw2.println(s1); pw3.println(s1); pw4.println(s1); 
								} else {
									String s1 = "Informacion: El jugador "+jugadorActual+" ha envidado "+envMas;
									pw1.println(s1); pw2.println(s1); pw3.println(s1); pw4.println(s1); 
								}
								if (jugadorActual == 3) {
									jugadorActual = 0;
								} else {
									jugadorActual++;
								}
							} else {

							}
						}
						if (n == 3) {
							e += envMas;
							pitasEnvidadas = e;
							String s1 = "Informacion: El jugador "+jugadorActual+" ha querido";
							pw1.println(s1); pw2.println(s1); pw3.println(s1); pw4.println(s1); 
							finalizado = true;
						}
						if (n == 4) {
							String s1 = "Informacion: El jugador "+jugadorActual+" no ha querido";
							pw1.println(s1); pw2.println(s1); pw3.println(s1); pw4.println(s1); 
							if (jugadorActual == 0 || jugadorActual == 1) {
								jugadorActual += 2;
								j = this.lJugadores.get(jugadorActual);
								if (j.tieneJuego()) {
									pw = new PrintWriter(this.escritores.get(jugadorActual),true);
									pw.println("El jugador " + jugadorActual + " habla [ 1: envido m�s, 2: quiero, 3: no quiero] " + j.mostrarCartas());
									dis = new DataInputStream(this.lectores.get(jugadorActual));
									int n4 = 0;
									try {
										n4 = Integer.parseInt(dis.readLine());
									} catch (NumberFormatException e1) {
										e1.printStackTrace();
									} catch (IOException e1) {
										e1.printStackTrace();
									}

									if (n4 == 1) {
										primerEnvite = false;
										pw.println("�Cuanto m�s quieres envidar?");
										try {
											envMas = Integer.parseInt(dis.readLine());
										} catch (NumberFormatException e1) {
											e1.printStackTrace();
										} catch (IOException e1) {
											e1.printStackTrace();
										}
										String s2 = "Informacion: El jugador "+jugadorActual+" ha envidado mas"+envMas;
										pw1.println(s2); pw2.println(s2); pw3.println(s2); pw4.println(s2); 
										e += envMas;
										if (jugadorActual == 3) {
											jugadorActual = 0;
										} else {
											jugadorActual++;
										}

									}
									if (n4 == 2) {
										String s2 = "Informacion: El jugador "+jugadorActual+" ha querido";
										pw1.println(s2); pw2.println(s2); pw3.println(s2); pw4.println(s2); 
										finalizado = true;
									}
									if (n4 == 3) {
										if (primerEnvite) {
											pitasEnvidadas = 1;
										} else {
											pitasEnvidadas = e;
										}
										String s2 = "Informacion: El jugador "+jugadorActual+" no ha querido";
										pw1.println(s2); pw2.println(s2); pw3.println(s2); pw4.println(s2); 
										finalizado = true;
									}
								} else {
									if (jugadorActual == 3) {
										jugadorActual = 0;
									} else {
										jugadorActual++;
									}
								}

							} else {
								jugadorActual -= 2;
								j = this.lJugadores.get(jugadorActual);
								if (j.tieneJuego()) {
									pw = new PrintWriter(this.escritores.get(jugadorActual),true);
									pw.println("El jugador " + jugadorActual + " habla [ 1: envido m�s, 2: quiero, 3: no quiero] " + j.mostrarCartas());
									dis = new DataInputStream(this.lectores.get(jugadorActual));
									int n4 = 0;
									try {
										n4 = Integer.parseInt(dis.readLine());
									} catch (NumberFormatException e1) {
										e1.printStackTrace();
									} catch (IOException e1) {
										e1.printStackTrace();
									}

									if (n4 == 1) {
										primerEnvite = false;
										pw.println("�Cuanto m�s quieres envidar?");
										try {
											envMas = Integer.parseInt(dis.readLine());
										} catch (NumberFormatException e1) {
											e1.printStackTrace();
										} catch (IOException e1) {
											e1.printStackTrace();
										}

										e += envMas;
										String s2 = "Informacion: El jugador "+jugadorActual+" ha envidado mas"+envMas;
										pw1.println(s2); pw2.println(s2); pw3.println(s2); pw4.println(s2); 
										if (jugadorActual == 3) {
											jugadorActual = 0;
										} else {
											jugadorActual++;
										}

									}
									if (n4 == 2) {
										String s2 = "Informacion: El jugador "+jugadorActual+" ha querido";
										pw1.println(s2); pw2.println(s2); pw3.println(s2); pw4.println(s2); 
										finalizado = true;
									}
									if (n4 == 3) {
										if (primerEnvite) {
											pitasEnvidadas = 1;
										} else {
											pitasEnvidadas = e;
										}
										String s2 = "Informacion: El jugador "+jugadorActual+" no ha querido";
										pw1.println(s2); pw2.println(s2); pw3.println(s2); pw4.println(s2); 
										finalizado = true;
									}
								} else {
									if (jugadorActual == 3) {
										jugadorActual = 0;
									} else {
										jugadorActual++;
									}
								}

							}
						}
					} else {
						jugadorActual++;
					}

				}
				return pitasEnvidadas;

			}
		}

	}
	//Juega el punto en el caso de que no se haya jugado el juego, y devulve el numero de pitasEnvidadas en la partida, que puede ser 0 en caso de que
    //todos los jugadores pasen, no enviden...
	public int punto() {

		Jugador j = null;
		int pitasEnvidadas = 0;
		boolean finalizado = false;
		int jugadorActual = 0;
		boolean primerEnvite = true;
		int e = 0;
		int envMas = 0;
		PrintWriter pw = null;
		DataInputStream dis = null;
		String s = "Informacion: PUNTO";
		pw1.println(s); pw2.println(s); pw3.println(s); pw4.println(s); 

		while (!finalizado && pitasEnvidadas < 25 && jugadorActual < 4) {

			j = this.lJugadores.get(jugadorActual);
			pw = new PrintWriter(this.escritores.get(jugadorActual),true);
			pw.println("El jugador " + jugadorActual + " habla [1: paso, 2: envido, 3: quiero, 4: no quiero] " + j.mostrarCartas());
			dis = new DataInputStream(this.lectores.get(jugadorActual));
			int n = 0;
			try {
				n = Integer.parseInt(dis.readLine());
			} catch (NumberFormatException e1) {
				e1.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			if (n == 1) {
				String s1 = "Informacion: El jugador "+jugadorActual+" ha pasado";
				pw1.println(s1); pw2.println(s1); pw3.println(s1); pw4.println(s1); 
				jugadorActual++;
			}
			if (n == 2) {
				pw.println("�Cuanto envidas?");

				if (primerEnvite) {
					try {
						e = Integer.parseInt(dis.readLine());
					} catch (NumberFormatException e1) {
						e1.printStackTrace();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
					primerEnvite = false;
				} else {
					try {
						envMas = Integer.parseInt(dis.readLine());
					} catch (NumberFormatException e1) {
						e1.printStackTrace();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}

				if (e < 25) {

					if (envMas == 0) {
						String s1 = "Informacion: El jugador "+jugadorActual+" ha envidado "+e;
						pw1.println(s1); pw2.println(s1); pw3.println(s1); pw4.println(s1); 
					} else {
						String s1 = "Informacion: El jugador "+jugadorActual+" ha envidado "+envMas;
						pw1.println(s1); pw2.println(s1); pw3.println(s1); pw4.println(s1); 
					}
					if (jugadorActual == 3) {
						jugadorActual = 0;
					} else {
						jugadorActual++;
					}
				} else {

				}
			}
			if (n == 3) {
				e += envMas;
				pitasEnvidadas = e;
				String s1 = "Informacion: El jugador "+jugadorActual+" ha querido";
				pw1.println(s1); pw2.println(s1); pw3.println(s1); pw4.println(s1); 
				finalizado = true;
			}
			if (n == 4) {
				String s1 = "Informacion: El jugador "+jugadorActual+" no ha querido";
				pw1.println(s1); pw2.println(s1); pw3.println(s1); pw4.println(s1); 
				if (jugadorActual == 0 || jugadorActual == 1) {
					jugadorActual += 2;
					j = this.lJugadores.get(jugadorActual);
					pw = new PrintWriter(this.escritores.get(jugadorActual));
					pw.println("El jugador " + jugadorActual + " habla [ 1: envido m�s, 2: quiero, 3: no quiero] " + j.mostrarCartas());
					dis = new DataInputStream(this.lectores.get(jugadorActual));
					int n4 = 0;
					try {
						n4 = Integer.parseInt(dis.readLine());
					} catch (NumberFormatException e1) {
						e1.printStackTrace();
					} catch (IOException e1) {
						e1.printStackTrace();
					}

					if (n4 == 1) {
						primerEnvite = false;
						pw.println("�Cuanto m�s quieres envidar?");
						try {
							envMas = Integer.parseInt(dis.readLine());
						} catch (NumberFormatException e1) {
							e1.printStackTrace();
						} catch (IOException e1) {
							e1.printStackTrace();
						}
						String s2 = "Informacion: El jugador "+jugadorActual+" ha envidado "+envMas;
						pw1.println(s2); pw2.println(s2); pw3.println(s2); pw4.println(s2); 
						e += envMas;
						if (jugadorActual == 3) {
							jugadorActual = 0;
						} else {
							jugadorActual++;
						}

					}
					if (n4 == 2) {
						String s2 = "Informacion: El jugador "+jugadorActual+" ha querido";
						pw1.println(s2); pw2.println(s2); pw3.println(s2); pw4.println(s2); 
						finalizado = true;
					}
					if (n4 == 3) {
						if (primerEnvite) {
							pitasEnvidadas = 1;
						} else {
							pitasEnvidadas = e;
						}
						String s2 = "Informacion: El jugador "+jugadorActual+" no ha querido";
						pw1.println(s2); pw2.println(s2); pw3.println(s2); pw4.println(s2); 
						finalizado = true;
					} else {
						if (jugadorActual == 3) {
							jugadorActual = 0;
						} else {
							jugadorActual++;
						}
					}
				} else {
					jugadorActual -= 2;
					j = this.lJugadores.get(jugadorActual);
					pw = new PrintWriter(this.escritores.get(jugadorActual),true);
					pw.println("El jugador " + jugadorActual + " habla [ 1: envido m�s, 2: quiero, 3: no quiero] " + j.mostrarCartas());
					dis = new DataInputStream(this.lectores.get(jugadorActual));
					int n4 = 0;
					try {
						n4 = Integer.parseInt(dis.readLine());
					} catch (NumberFormatException e1) {
						e1.printStackTrace();
					} catch (IOException e1) {
						e1.printStackTrace();
					}

					if (n4 == 1) {
						primerEnvite = false;
						pw.println("�Cuanto m�s quieres envidar?");
						try {
							envMas = Integer.parseInt(dis.readLine());
						} catch (NumberFormatException e1) {
							e1.printStackTrace();
						} catch (IOException e1) {
							e1.printStackTrace();
						}
						String s2 = "Informacion: El jugador "+jugadorActual+" ha envidado "+envMas;
						pw1.println(s2); pw2.println(s2); pw3.println(s2); pw4.println(s2); 
						e += envMas;
						if (jugadorActual == 3) {
							jugadorActual = 0;
						} else {
							jugadorActual++;
						}

					}
					if (n4 == 2) {
						String s2 = "Informacion: El jugador "+jugadorActual+" ha querido";
						pw1.println(s2); pw2.println(s2); pw3.println(s2); pw4.println(s2); 
						finalizado = true;
					}
					if (n4 == 3) {
						if (primerEnvite) {
							pitasEnvidadas = 1;
						} else {
							pitasEnvidadas = e;
						}
						String s2 = "Informacion: El jugador "+jugadorActual+" no ha querido";
						pw1.println(s2); pw2.println(s2); pw3.println(s2); pw4.println(s2); 
						finalizado = true;
					}
				}

			}

		}

		return pitasEnvidadas;

	}

}
